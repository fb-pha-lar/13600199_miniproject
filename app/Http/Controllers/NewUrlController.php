<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\URLShort;

class NewUrlController extends Controller
{
    //
    public function short(Request $request){
        $url = URLShort::whereUrl($request->url)->first();
        if ($url == null){
            $short = $this->generateShortURL();
            URLShort::create([
                'url' => $request->url,
                'short' => $short

            ]);

            $url = URLShort::whereUrl($request->url)->first();

        }

        return view('url.short_url',compact('url'));
    }

    public function shortlink($link){
        $url = URLShort::whereShort($link)->first();
        return redirect($url->url);
    }

    public function generateShortURL(){
        $result = (rand(1000,99999).chr(rand(65,90)));
        $data = URLShort::whereShort($result)->first();

        if ($data != null){
            $this->generateShortURL();
        }
        session()->flash('notif','Success to save '."www.short.local/".$result); //notification

        return $result;
    }

}
