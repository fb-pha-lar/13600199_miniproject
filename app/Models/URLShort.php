<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class URLShort extends Model
{
    //
    protected $table = 'short';
    protected $primaryKey = 'id';

    protected $fillable = [
        'url',
        'short'
    ];
}
