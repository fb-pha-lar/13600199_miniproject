@extends('layouts.app')
@section('content')
    <hr>
    <form method="post" action="{{ url('/todo) }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <button type="button" class="btn btn-primary col-md-4"  >New post</button>
            <button type="button" class="btn btn-info col-md-4 offset-md-4"  >View all post</button>
        </div>
        <h1>New post</h1>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Photo</label>
            <div class="col-sm-10">
                <input type="file" name="file" class="form-control">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <input type="text" name="longurl" class="form-control" value="{{  '' }}">
            </div>
        </div>

        {{--        <div class="form-group">--}}
        {{--            <label>Due</label>--}}
        {{--            <input type="date" name="due" class="form-control" value="{{ old('due') }}">--}}
        {{--        </div>--}}

        <button type="submit" class="btn btn-success">Upload & Post</button>
    </form>
@endsection
