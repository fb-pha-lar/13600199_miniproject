<?php $__env->startSection('content'); ?>
    <hr>
    <form method="post" action="<?php echo e(url('/todo)); ?>" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="row">
            <button type="button" class="btn btn-primary col-md-4"  >New post</button>
            <button type="button" class="btn btn-info col-md-4 offset-md-4"  >View all post</button>
        </div>
        <h1>New post</h1>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Photo</label>
            <div class="col-sm-10">
                <input type="file" name="file" class="form-control">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <input type="text" name="longurl" class="form-control" value="<?php echo e(''); ?>">
            </div>
        </div>

        
        
        
        

        <button type="submit" class="btn btn-success">Upload & Post</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> ?><?php /**PATH C:\xampp\htdocs\short\resources\views/newshort.blade.php ENDPATH**/ ?>